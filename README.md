# Decision Jam App
This is the official repository for the Decision Jam App. It is being 
developed using Flutter and Firebase.

To work on the app, you need the files `key.properties` and `key_store.jks`.
Ask your supervisor in order to receive them as they should not be checked
into public source control.

