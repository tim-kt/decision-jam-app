/// Provides technical parameters for a Decision Jam session
class Session {
  /// The code that is used to connect the app to Firestore
  final String code;
  /// Whether or not the current user is a moderator or participant
  final bool moderator;

  /// The duration of the wait, write and vote segments
  Duration waitDuration = Duration(seconds: 10);
  Duration writeDuration = Duration(minutes: 2);
  Duration voteDuration = Duration(minutes: 2);

  Session(this.code, this.moderator);
}