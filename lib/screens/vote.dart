import 'dart:async';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:decisionjamapp/util/synchronizer.dart';
import 'package:decisionjamapp/util/synchronizable.dart';
import 'package:decisionjamapp/util/widget_arguments.dart';
import 'package:decisionjamapp/widgets/progress_indicator_container.dart';
import 'package:decisionjamapp/widgets/information_card.dart';
import 'package:decisionjamapp/widgets/information_container.dart';

class VoteScaffold extends StatefulWidget {
  static const routeName = '/vote';

  @override
  _VoteScaffoldState createState() => _VoteScaffoldState();
}

class _VoteScaffoldState extends State<VoteScaffold>
    with SingleTickerProviderStateMixin
    implements Synchronizable {
  int _votesAvailable = 4;
  List<Map<String, dynamic>> _objects;
  bool _buttonsEnabled = true;

  @override
  void initState() {
    super.initState();
    Synchronizer.instance.add(this);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final WidgetArguments args = ModalRoute.of(context).settings.arguments;
    Timer(args.end.difference(DateTime.now()), () {
      Synchronizer.instance.notify();
    });
  }

  @override
  Widget build(BuildContext context) {
    final WidgetArguments args = ModalRoute.of(context).settings.arguments;
    if (_objects == null) {
      _getObjects(Synchronizer.instance.session, args.task);
    }
    Scaffold scaffold = Scaffold(
      appBar: AppBar(
        title: Text('Decision Jam App'),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        children: <Widget>[
          _getTask(args.task),
          ProgressIndicatorContainer(args.start, args.end),
          Expanded(
            child: Container(
              child: ListView.builder(
                itemCount: (_objects != null) ? _objects.length : 0,
                itemBuilder: (context, position) {
                  return Card(
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(_objects[position]['content']),
                          Divider(),
                          Row(
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: IconButton(
                                  onPressed: _buttonsEnabled
                                      ? () {
                                          if (_objects[position]['votes'] > 0) {
                                            setState(() {
                                              _votesAvailable++;
                                              _objects[position]['votes']--;
                                            });
                                            _castVote(
                                                Synchronizer.instance.session,
                                                args.task,
                                                _objects[position],
                                                -1);
                                          }
                                        }
                                      : null,
                                  icon: Icon(
                                    Icons.remove_circle_outline,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 8,
                                child: Text(
                                  _objects[position]['votes'].toString(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 18.0),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: IconButton(
                                  onPressed: _buttonsEnabled
                                      ? () {
                                          if (_votesAvailable > 0) {
                                            setState(() {
                                              _votesAvailable--;
                                              _objects[position]['votes']++;
                                            });
                                            _castVote(
                                                Synchronizer.instance.session,
                                                args.task,
                                                _objects[position],
                                                1);
                                          }
                                        }
                                      : null,
                                  icon: Icon(Icons.add_circle_outline,
                                      color: Colors.green),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          InformationContainer('Votes left: ' + _votesAvailable.toString()),
        ],
      ),
    );
    return WillPopScope(
      onWillPop: () async => false,
      child: scaffold,
    );
  }

  @override
  void synchronize(String routeName, String task,
      [DateTime start, DateTime end]) {
    Navigator.pushReplacementNamed(context, routeName,
        arguments: WidgetArguments(task, start, end));
  }

  void _getObjects(session, task) async {
    /// Get objects from Firestore
    await Firestore.instance
        .collection('sessions/' + session.code + '/' + task)
        .getDocuments()
        .then((QuerySnapshot qs) {
      _objects = List<Map<String, dynamic>>();
      qs.documents.forEach((ds) {
        _objects.add({
          'documentID': ds.documentID,
          'content': ds.data['content'],
          'votes': ds.data['votes']
        });
      });
      setState(() {});
    });
  }

  /// Updates a given [object] by [vote] (1 or -1) in the Firestore database.
  void _castVote(session, task, object, vote) async {
    Firestore.instance
        .collection('sessions/' + session.code + '/' + task)
        .document(object['documentID'])
        .updateData(<String, dynamic>{
      'votes': FieldValue.increment(vote),
    });
  }

  Widget _getTask(placeholder) {
    String task;
    if (placeholder == 'problems') {
      task =
          'Please vote on all the problems you consider important to solve in your team.';
    }
    if (placeholder == 'solutions') {
      task = 'Please vote on all the solutions you consider good.';
    }
    return InformationCard(
      task,
      2.0,
    );
  }
}

class Vote {
  final String text;
  final int vote;

  Vote(this.text, this.vote);
}
