import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:decisionjamapp/util/synchronizable.dart';
import 'package:decisionjamapp/util/synchronizer.dart';
import 'package:decisionjamapp/util/widget_arguments.dart';

class WaitScaffold extends StatefulWidget {
  static const routeName = '/wait';

  @override
  _WaitScaffoldState createState() => _WaitScaffoldState();
}

class _WaitScaffoldState extends State<WaitScaffold> implements Synchronizable {
  Ticker _progressUpdater;
  String _timeLeft;

  @override
  void initState() {
    super.initState();
    Synchronizer.instance.add(this);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final WidgetArguments args = ModalRoute.of(context).settings.arguments;
    _progressUpdater = Ticker((elapsed) {
      if (mounted) {
        setState(() {
          _timeLeft = _getTime(args.end.difference(DateTime.now()));
        });
      }
    });
    _progressUpdater.start();
    Timer(args.end.difference(DateTime.now()), () {
      Synchronizer.instance.notify();
    });
  }


  @override
  Widget build(BuildContext context) {
    final WidgetArguments args = ModalRoute.of(context).settings.arguments;
    Scaffold scaffold = Scaffold(
      appBar: AppBar(
        title: Text('Decision Jam App'),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Center(
              child: _getTask(args.task),
            ),
          ),
          Expanded(
            flex: 3,
            child: Center(
              child: Text(
                _timeLeft ?? '',
                style: TextStyle(fontSize: 36.0),
              ),
            ),
          ),
        ],
      ),
    );
    return WillPopScope(
      onWillPop: () async => false,
      child: scaffold,
    );
  }

  @override
  void dispose() {
    _progressUpdater.dispose();
    super.dispose();
  }

  @override
  void synchronize(
      String routeName, String task, [DateTime start, DateTime end]) {
    Navigator.pushReplacementNamed(context, routeName,
        arguments: WidgetArguments(task, start, end));
  }

  String _getTime(Duration duration){
    if (duration.isNegative) {
      return '0:00';
    }
    String time = duration.inMinutes.toString() + ':';
    if (duration.inSeconds - duration.inMinutes*60 < 10){
      time += '0';
    }
    time += (duration.inSeconds - duration.inMinutes*60).toString();
    return time;
  }

  Widget _getTask(placeholder){
    String task;
    if (placeholder == 'problemwriting'){
      task = 'Writing problems';
    }
    if (placeholder == 'problemvoting'){
      task = 'Voting on problems';
    }
    if (placeholder == 'solutionwriting'){
      task = 'Writing solutions';
    }
    if (placeholder == 'solutionvoting') {
      task = 'Voting on solutions';
    }
    return Text(task + ' starts in');
  }
}
