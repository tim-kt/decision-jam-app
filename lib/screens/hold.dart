import 'package:flutter/material.dart';

import 'package:decisionjamapp/util/synchronizable.dart';
import 'package:decisionjamapp/util/synchronizer.dart';
import 'package:decisionjamapp/util/widget_arguments.dart';

class HoldScaffold extends StatefulWidget {
  static const routeName = '/hold';

  @override
  _HoldScaffoldState createState() => _HoldScaffoldState();
}

class _HoldScaffoldState extends State<HoldScaffold> implements Synchronizable {
  @override
  void initState() {
    super.initState();
    Synchronizer.instance.add(this);
  }

  @override
  Widget build(BuildContext context) {
    final WidgetArguments args = ModalRoute.of(context).settings.arguments;
    Scaffold scaffold = Scaffold(
      appBar: AppBar(
        title: Text('Decision Jam App'),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Center(
              child: _getTask(args.task),
            ),
          ),
          Expanded(
            flex: 3,
            child: Center(
              child: Text(
                'Please stand by!',
              ),
            ),
          ),
        ],
      ),
    );
    return WillPopScope(
      onWillPop: () async => false,
      child: scaffold,
    );
  }

  @override
  void synchronize(String routeName, String task,
      [DateTime start, DateTime end]) {
    Navigator.pushReplacementNamed(context, routeName,
        arguments: WidgetArguments(task, start, end));
  }

  Widget _getTask(placeholder) {
    String task;
    if (placeholder == 'start') {
      task = 'The moderator will shortly start the session.';
    }
    if (placeholder == 'hmw') {
      task = 'The moderator is currently rewriting the most voted problem.';
    }
    return Text(
      task,
      textAlign: TextAlign.center,
    );
  }
}
