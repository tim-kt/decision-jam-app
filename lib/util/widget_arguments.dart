/// Used to pass arguments through when pushing widgets over a route.
class WidgetArguments {
  /// A description or text for the current task
  final String task;
  /// The time the current task starts
  final DateTime start;
  /// The time the current task ends
  final DateTime end;

  WidgetArguments(this.task, [this.start, this.end]);
}