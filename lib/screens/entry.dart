import 'dart:math';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:decisionjamapp/util/session.dart';
import 'package:decisionjamapp/util/synchronizer.dart';
import 'package:decisionjamapp/util/synchronizable.dart';
import 'package:decisionjamapp/util/widget_arguments.dart';
import 'package:decisionjamapp/widgets/default_appbar.dart';

/// Provides the [_moderator] and [_code] arguments for [EntryScaffold].
class EntryArguments {
  final bool _moderator;

  EntryArguments(this._moderator);
}

class EntryScreen extends StatefulWidget {
  static const routeName = '/entry';

  @override
  _EntryScreenState createState() => _EntryScreenState();
}

class _EntryScreenState extends State<EntryScreen>
    implements Synchronizable {
  bool _enabled = false;
  String _submittedCode;
  Session session;

  @override
  void initState() {
    super.initState();
    Synchronizer.instance.add(this);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final EntryArguments args = ModalRoute.of(context).settings.arguments;
    if (args._moderator) {
      if (session == null) {
        session = _initializeSession();
        Synchronizer.instance.session = session;
        _getConfig();
        _enabled = true;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final EntryArguments args = ModalRoute.of(context).settings.arguments;

    return WillPopScope(
      onWillPop: () async {
        if (args._moderator) {
          _deleteSession(session);
        }
        Synchronizer.instance.pop();
        return true;
      },
      child: Scaffold(
        appBar: DefaultAppBar(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: args._moderator
                ? [
                    _buildInstructions(
                        'Tell your participants to enter the following code:'),
                    _buildCodeOutput(session.code ?? ''),
                    _buildButton(
                        'Start session', _enabled ? _startSession : null)
                  ]
                : [
                    _buildInstructions(
                        'Enter the code you have received from your moderator:'),
                    _buildCodeInput(),
                    _buildButton('Submit code', _enabled ? _submitCode : null),
                  ],
          ),
        ),
        floatingActionButton: args._moderator ? _buildAdjustButton() : null,
      ),
    );
  }

  /// Returns a [SizedBox] horizontally centered with the width 256.0
  /// containing [text]
  SizedBox _buildInstructions(text) {
    return SizedBox(
      width: 256.0,
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 18.0,
        ),
      ),
    );
  }

  /// Returns a [Card] showing [code]
  Card _buildCodeOutput(code) {
    return Card(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 12.0),
        child: Text(
          code,
          style: TextStyle(
            fontSize: 24.0,
            letterSpacing: 4.0,
          ),
        ),
      ),
    );
  }

  /// Returns a [ButtonTheme] containing a blue [RaisedButton] with the text
  /// [text] and the function [onPressed]
  ButtonTheme _buildButton(text, onPressed) {
    return ButtonTheme(
      minWidth: 180.0,
      height: 60.0,
      child: RaisedButton(
        child: Text(text, style: TextStyle(fontSize: 18)),
        color: Colors.blue[600],
        textColor: Colors.white,
        padding: const EdgeInsets.all(0.0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(32.0))),
        onPressed: onPressed,
      ),
    );
  }

  /// Returns an extended [FloatingActionButton] with the text "Adjust",
  /// leading to the settings screen
  FloatingActionButton _buildAdjustButton() {
    return FloatingActionButton.extended(
      onPressed: () {
        Navigator.pushNamed(context, '/session_settings');
      },
      icon: Icon(Icons.settings),
      label: Text('Adjust'),
      backgroundColor: Colors.blue[600],
    );
  }

  Card _buildCodeInput() {
    return Card(
      child: SizedBox(
        width: 140.0,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 12.0),
          child: TextField(
            style: TextStyle(
              fontSize: 24.0,
              letterSpacing: 4.0,
            ),
            decoration: InputDecoration(
              isDense: true,
              contentPadding: EdgeInsets.all(0.0),
            ),
            textAlign: TextAlign.center,
            onChanged: (text) {
              setState(() {
                if (text.length == 6) {
                  _enabled = true;
                } else {
                  _enabled = false;
                }
                _submittedCode = text;
              });
            },
          ),
        ),
      ),
    );
  }

  @override
  void synchronize(String routeName, String task,
      [DateTime start, DateTime end]) {
    Navigator.pushReplacementNamed(context, routeName,
        arguments: WidgetArguments(task, start, end));
  }

  /// Generates a code containing [digits] random digits.
  ///
  /// Collects all [nouns] from the english_words package, that have 3 or 4
  /// letters, shuffles the created [shortNouns] and converts the joined String
  /// to lower case.
  /// TODO check if a session with this code already exists
  String _getRandomCode(digits) {
    var rng = new Random();
    String result = '';
    for (int i = 0; i < digits; i++) {
      result += rng.nextInt(10).toString();
    }
    return result;
  }

  /// Connects a participant to a session
  void _submitCode() {
    session = Session(_submittedCode, false);
    Synchronizer.instance.session = session;
    Synchronizer.instance.listen();
  }

  /// Initializes a session and returns a [Session] object.
  Session _initializeSession() {
    Session session = Session(_getRandomCode(6), true);
    Firestore.instance.collection('sessions').document(session.code).setData({
      'on_hold': 'start',
    });

    Synchronizer.instance.session = session;
    Synchronizer.instance.listen();
    return session;
  }

  /// Starts the session by initializing the first part of the decision jam
  void _startSession() {
    print("click");
    setState(() {
      _enabled = false;
    });
    DateTime now = DateTime.now();
    DateTime waitProblemwritingStart = now;
    DateTime waitProblemwritingEnd =
        waitProblemwritingStart.add(session.waitDuration);
    DateTime writeProblemsStart = waitProblemwritingEnd;
    DateTime writeProblemsEnd = writeProblemsStart.add(session.writeDuration);
    DateTime waitProblemvotingStart = writeProblemsEnd;
    DateTime waitProblemvotingEnd =
        waitProblemvotingStart.add(session.waitDuration);
    DateTime voteProblemsStart = waitProblemvotingEnd;
    DateTime voteProblemsEnd = voteProblemsStart.add(session.voteDuration);

    Firestore.instance.collection('sessions').document(session.code).updateData(
      <String, dynamic>{
        'timestamps': {
          'wait_problemwriting': {
            'start': waitProblemwritingStart,
            'end': waitProblemwritingEnd,
          },
          'write_problems': {
            'start': writeProblemsStart,
            'end': writeProblemsEnd,
          },
          'wait_problemvoting': {
            'start': waitProblemvotingStart,
            'end': waitProblemvotingEnd,
          },
          'vote_problems': {
            'start': voteProblemsStart,
            'end': voteProblemsEnd,
          },
        },
        'on_hold': 'hmw',
      },
    );
  }

  void _deleteSession(session) {
    Firestore.instance.collection('sessions').document(session.code).delete();
  }

  void _getConfig() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('current_config')) {
      session.waitDuration = Duration(seconds: prefs.getInt('current_wait'));
      session.writeDuration = Duration(seconds: prefs.getInt('current_write'));
      session.voteDuration = Duration(seconds: prefs.getInt('current_vote'));
    }
  }
}
