/// Widgets implementing this class can be synchronized by [Synchronizer].
///
/// Used by the app to wait synchronize the app's current state with
/// the database while supporting changes in the database (for example when the
/// moderator extends the duration of the problem writing section, because
/// a participant had technical issues).
///
/// Implements a variation of the Observer pattern.
abstract class Synchronizable {
  void synchronize(String routeName, String task, [DateTime start, DateTime end]);
}