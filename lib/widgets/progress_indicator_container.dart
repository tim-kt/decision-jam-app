import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

/// Combines a LinearProgressIndicator and a Text as countdown in a container
/// to display how much time there is left for a specific task.
///
/// Takes a [_start] and an [_end] DateTime
/// Can be completely rebuild if the [_end] changes.
class ProgressIndicatorContainer extends StatefulWidget {
  /// The time when the task started
  final DateTime _start;

  /// The time when the task is supposed to end
  final DateTime _end;

  ProgressIndicatorContainer(this._start, this._end);

  @override
  _ProgressIndicatorContainerState createState() =>
      _ProgressIndicatorContainerState();
}

/// The state of [ProgressIndicatorContainer].
///
/// The ticker [_progressUpdater] updates [_timeLeft] and [_value] every frame.
class _ProgressIndicatorContainerState
    extends State<ProgressIndicatorContainer> {
  Ticker _progressUpdater;

  /// The time left for the task in a human friendly format (for example 01:40 or 07:20)
  String _timeLeft;

  /// The value for the [LinearProgressIndicator].
  double _value;

  /// Initializes the [_progressUpdater].
  ///
  /// Every frame [_timeLeft] and [_value] are updated using the methods
  /// [_getTimeLeft] and [_getValue].
  @override
  void initState() {
    super.initState();
    _progressUpdater = Ticker((elapsed) {
      if (mounted) {
        setState(() {
          if (_timeLeft != '00:00') {
            _timeLeft = _getTime(widget._end.difference(DateTime.now()));
          }
          _value = _getValue();
        });
      }
    });
    _progressUpdater.start();
  }

  /// Builds the container widget.
  ///
  /// Uses a [Column] to separate the [LinearProgressIndicator] from the [Text]
  /// below. The text centered is inside of a [Container] with a margin.
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: Column(
        children: <Widget>[
          LinearProgressIndicator(value: _value),
          Container(
            margin: EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                _timeLeft ?? '',
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Disposes the [_progressUpdater]
  @override
  void dispose() {
    _progressUpdater.dispose();
    super.dispose();
  }

  /// Generates minutes and seconds left for the task
  ///
  /// ```dart
  /// _getTimeLeft() == '1:40'
  /// ```

  String _getTime(Duration duration) {
    if (duration.isNegative) {
      return '0:00';
    }
    String time = duration.inMinutes.toString() + ':';
    if (duration.inSeconds - duration.inMinutes * 60 < 10) {
      time += '0';
    }
    time += (duration.inSeconds - duration.inMinutes * 60).toString();
    return time;
  }

  /// Generates a double value for the [LinearProgressIndicator].
  ///
  /// The duration of the task is [_endTime] - [_startTime],
  /// the elapsed time is the time now - [_startTime]. These two divided give
  /// the percentage of how much time of the task is elapsed, subtracting this
  /// from 1.0 results in the time remaining.
  ///
  /// ```dart
  /// _getValue() == 0.2
  /// ```
  double _getValue() {
    return 1.0 -
        (DateTime.now().millisecondsSinceEpoch -
                widget._start.millisecondsSinceEpoch) /
            (widget._end.millisecondsSinceEpoch -
                widget._start.millisecondsSinceEpoch);
  }
}
