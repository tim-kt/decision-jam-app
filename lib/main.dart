import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:decisionjamapp/screens/menu.dart';
import 'package:decisionjamapp/screens/entry.dart';
import 'package:decisionjamapp/screens/session_settings.dart';
import 'package:decisionjamapp/screens/hold.dart';
import 'package:decisionjamapp/screens/wait.dart';
import 'package:decisionjamapp/screens/write.dart';
import 'package:decisionjamapp/screens/vote.dart';
import 'package:decisionjamapp/screens/hmw.dart';
import 'package:decisionjamapp/screens/results.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new Launcher());
  });
}

class Launcher extends StatelessWidget {
  // The duration of different sections
  static const Duration waitDuration = Duration(seconds: 5);
  static const Duration writeDuration = Duration(seconds: 10);
  static const Duration voteDuration = Duration(seconds: 10);
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => MenuScaffold(),
        EntryScreen.routeName: (context) => EntryScreen(),
        SessionSettingsScaffold.routeName: (context) => SessionSettingsScaffold(),
        HoldScaffold.routeName: (context) => HoldScaffold(),
        WaitScaffold.routeName: (context) => WaitScaffold(),
        WriteScaffold.routeName: (context) => WriteScaffold(),
        VoteScaffold.routeName: (context) => VoteScaffold(),
        HMWScaffold.routeName: (context) => HMWScaffold(),
        ResultsScaffold.routeName: (context) => ResultsScaffold(),
      },
      title: 'Decision Jam App',
    );
  }
}
