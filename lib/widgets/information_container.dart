import 'package:flutter/material.dart';

/// Light-grey [Container] displaying a single string [information].
///
/// For example used to display how many votes a user has left.
class InformationContainer extends StatelessWidget{
  /// String to display
  final String information;

  InformationContainer(this.information);

  /// Builds the [Container] with a light-grey background and a centered,
  /// "padded" [Text] with the fontSize 18.0.
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: Center(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Text(
            information,
            style: TextStyle(fontSize: 20.0),
          ),
        ),
      ),
    );
  }
}