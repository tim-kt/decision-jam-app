import 'dart:collection';

import 'package:decisionjamapp/util/synchronizer.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:decisionjamapp/util/widget_arguments.dart';
import 'package:decisionjamapp/widgets/information_card.dart';

class ResultsScaffold extends StatefulWidget {
  static const routeName = '/results';

  @override
  _ResultsScaffoldState createState() => _ResultsScaffoldState();
}

class _ResultsScaffoldState extends State<ResultsScaffold> {
  LinkedHashMap<String, int> _objects;

  @override
  void initState() {
    super.initState();
    _getObjects(Synchronizer.instance.session);
  }

  @override
  Widget build(BuildContext context) {
    final WidgetArguments args = ModalRoute.of(context).settings.arguments;
    Scaffold scaffold = Scaffold(
      appBar: AppBar(
        title: Text('Decision Jam App'),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        children: <Widget>[
          _getTask(args.task),
          Expanded(
            child: Container(
              child: ListView.builder(
                itemCount: (_objects != null) ? _objects.length : 0,
                itemBuilder: (context, position) {
                  return Card(
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(_objects.keys.toList()[position]),
                          Divider(),
                          Center(
                            child: Text(
                              _objects.values.toList()[position].toString(),
                              style: TextStyle(fontSize: 18.0),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
    return WillPopScope(
      onWillPop: () async => false,
      child: scaffold,
    );
  }

  void _getObjects(session) async {
    /// Get objects from Firestore
    await Firestore.instance
        .collection('sessions/' + session.code + '/solutions')
        .getDocuments()
        .then((qs) {
      Map<String, int> solutions = {};
      qs.documents.forEach((ds) {
        solutions[ds.data['content']] = ds.data['votes'];
      });
      LinkedHashMap<String, int> sortedSolutions = LinkedHashMap();
      int highest = 0;
      solutions.values.forEach((value) {
        if (value > highest) {
          highest = value;
        }
      });

      while (solutions.length != sortedSolutions.length) {
        solutions.forEach((key, value){
          if (value == highest){
            sortedSolutions[key] = value;
          }
        });
        highest--;
      }

      setState(() {
        _objects = sortedSolutions;
      });
    });
  }

  Widget _getTask(placeholder) {
    String task;
    if (placeholder == 'results') {
      task = 'Congratulations, you\'re done! Here are the results:';
    }
    return InformationCard(
      task,
      2.0,
    );
  }
}
