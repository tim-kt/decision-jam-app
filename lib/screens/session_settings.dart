import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:decisionjamapp/util/synchronizer.dart';

class SessionSettingsScaffold extends StatefulWidget {
  static const routeName = '/session_settings';

  @override
  _SessionSettingsScaffoldState createState() =>
      _SessionSettingsScaffoldState();
}

class _SessionSettingsScaffoldState extends State<SessionSettingsScaffold> {
  String currentConfig = 'demo';

  Map<String, Map<String, Duration>> settings = {
    'wait': {
      'rapid': Duration(seconds: 5),
      'demo': Duration(seconds: 10),
      'custom': null,
    },
    'write': {
      'rapid': Duration(seconds: 10),
      'demo': Duration(minutes: 2),
      'custom': null,
    },
    'vote': {
      'rapid': Duration(seconds: 10),
      'demo': Duration(minutes: 2),
      'custom': null,
    },
  };

  @override
  void initState() {
    super.initState();
    _getConfig();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Decision Jam App'),
          backgroundColor: Colors.blue[600],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    OutlineButton(
                        onPressed: () {
                          setState(() {
                            currentConfig = 'rapid';
                          });
                        },
                        child: Text(
                          'Rapid',
                          style: TextStyle(
                              color: (currentConfig == 'rapid')
                                  ? Colors.grey[900]
                                  : Colors.grey),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(32.0),
                                bottomLeft: Radius.circular(32.0))),
                        borderSide: BorderSide(color: Colors.grey)),
                    OutlineButton(
                      onPressed: () {
                        setState(() {
                          currentConfig = 'demo';
                        });
                      },
                      child: Text(
                        'Demo',
                        style: TextStyle(
                            color: (currentConfig == 'demo')
                                ? Colors.grey[900]
                                : Colors.grey),
                      ),
                      borderSide: BorderSide(color: Colors.grey),
                    ),
                    OutlineButton(
                      onPressed: () {
                        setState(() {
                          currentConfig = 'custom';
                        });
                      },
                      child: Text(
                        'Custom',
                        style: TextStyle(
                            color: (currentConfig == 'custom')
                                ? Colors.grey[900]
                                : Colors.grey),
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(32.0),
                              bottomRight: Radius.circular(32.0))),
                      borderSide: BorderSide(color: Colors.grey),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: Column(
                children: <Widget>[
                  Divider(),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Center(
                            child: Text('Wait duration'),
                          ),
                        ),
                        Expanded(
                          child: Center(
                            child: _buildTimeView('wait'),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Center(
                            child: Text('Write duration'),
                          ),
                        ),
                        Expanded(
                          child: Center(
                            child: _buildTimeView('write'),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Center(
                            child: Text('Vote duration'),
                          ),
                        ),
                        Expanded(
                          child: Center(
                            child: _buildTimeView('vote'),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                ],
              ),
            ),
            Expanded(
                flex: 3,
                child: Center(
                  child: ButtonTheme(
                    minWidth: 180.0,
                    height: 60.0,
                    child: RaisedButton(
                      child: Text('Done', style: TextStyle(fontSize: 18)),
                      color: Colors.blue[600],
                      textColor: Colors.white,
                      padding: const EdgeInsets.all(0.0),
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(32.0))),
                      onPressed: _adjust,
                    ),
                  ),
                )),
          ],
        ),
      ),
      onWillPop: () async {
       _adjust();
       return false;
      }
    );
  }

  Widget _buildBottomPicker(Widget picker) {
    return Container(
      height: 216.0,
      padding: const EdgeInsets.only(top: 6.0),
      color: CupertinoColors.white,
      child: DefaultTextStyle(
        style: const TextStyle(
          color: CupertinoColors.black,
          fontSize: 22.0,
        ),
        child: GestureDetector(
          // Blocks taps from propagating to the modal sheet and popping.
          onTap: () {},
          child: SafeArea(
            top: false,
            child: picker,
          ),
        ),
      ),
    );
  }

  String _getTime(Duration duration) {
    String time = duration.inMinutes.toString() + ':';
    if (duration.inSeconds - duration.inMinutes * 60 < 10) {
      time += '0';
    }
    time += (duration.inSeconds - duration.inMinutes * 60).toString();
    return time;
  }

  Widget _buildTimeView(String key) {
    return (currentConfig == 'custom')
        ? GestureDetector(
            onTap: () {
              showCupertinoModalPopup<void>(
                context: context,
                builder: (BuildContext context) {
                  return _buildBottomPicker(
                    CupertinoTimerPicker(
                      mode: CupertinoTimerPickerMode.ms,
                      initialTimerDuration: settings[key][currentConfig],
                      onTimerDurationChanged: (Duration newTimer) {
                        setState(() => settings[key][currentConfig] = newTimer);
                      },
                    ),
                  );
                },
              );
            },
            child: Text(
              _getTime(settings[key][currentConfig] ?? ''),
              style: TextStyle(
                  decoration: TextDecoration.underline, fontSize: 22.0),
            ),
          )
        : Text(
            _getTime(settings[key][currentConfig]),
            style: TextStyle(fontSize: 22.0),
          );
  }

  void _getConfig() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('current_config')) {
      setState(() {
        currentConfig = prefs.getString('current_config');
        settings['wait']['custom'] =
            Duration(seconds: prefs.getInt('custom_wait'));
        settings['write']['custom'] =
            Duration(seconds: prefs.getInt('custom_write'));
        settings['vote']['custom'] =
            Duration(seconds: prefs.getInt('custom_vote'));
      });

    } else {
      setState(() {
        settings['wait']['custom'] = Duration(seconds: 15);
        settings['write']['custom'] = Duration(minutes: 5);
        settings['vote']['custom'] = Duration(minutes: 5);
      });
    }
  }

  void _adjust() async {
    Synchronizer.instance.session.waitDuration =
        settings['wait'][currentConfig];
    Synchronizer.instance.session.writeDuration =
        settings['write'][currentConfig];
    Synchronizer.instance.session.voteDuration =
        settings['vote'][currentConfig];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('custom_wait', settings['wait']['custom'].inSeconds);
    await prefs.setInt('custom_write', settings['write']['custom'].inSeconds);
    await prefs.setInt('custom_vote', settings['vote']['custom'].inSeconds);
    await prefs.setInt(
        'current_wait', settings['wait'][currentConfig].inSeconds);
    await prefs.setInt(
        'current_write', settings['write'][currentConfig].inSeconds);
    await prefs.setInt(
        'current_vote', settings['vote'][currentConfig].inSeconds);
    await prefs.setString('current_config', currentConfig);
    Navigator.pop(context);
  }
}
