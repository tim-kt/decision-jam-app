import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:decisionjamapp/util/synchronizer.dart';
import 'package:decisionjamapp/util/synchronizable.dart';
import 'package:decisionjamapp/util/widget_arguments.dart';
import 'package:decisionjamapp/widgets/information_card.dart';

class HMWScaffold extends StatefulWidget {
  static const routeName = '/hmw';

  @override
  _HMWScaffoldState createState() => _HMWScaffoldState();
}

class _HMWScaffoldState extends State<HMWScaffold> implements Synchronizable {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    Synchronizer.instance.add(this);
  }

  @override
  Widget build(BuildContext context) {
    Scaffold scaffold = Scaffold(
      appBar: AppBar(
        title: Text('Decision Jam App'),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 64.0),
                child: Text(
                  'Please rewrite this problem into the "How might we" format',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18.0),
                ),
              ),
            ),
          ),
          StreamBuilder(
              stream: Firestore.instance
                  .collection('sessions/' +
                      Synchronizer.instance.session.code +
                      '/problems')
                  .snapshots(),
              builder: (context, snapshot) {
                String problem;
                int votes = 0;
                if (snapshot.hasData) {
                  snapshot.data.documents.forEach((ds) {
                    if (ds.data['votes'] >= votes) {
                      problem = ds.data['content'];
                      votes = ds.data['votes'];
                    }
                  });
                }
                return InformationCard(problem, 1.0);
              }),
          Expanded(child: SizedBox()),
          Container(
            color: Colors.grey[200],
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: TextField(
                  controller: _controller,
                  onEditingComplete: () {
                    if (_controller.text.isNotEmpty) {
                      // TODO Pop-Up "Are you sure"
                      _addHMW(Synchronizer.instance.session, _controller.text);
                      _continueSession(Synchronizer.instance.session);
                    }
                  },
                  minLines: 1,
                  maxLines: 5,
                  textInputAction: TextInputAction.send,
                  decoration: InputDecoration(
                    hintText: 'Type here...',
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
    return WillPopScope(
      onWillPop: () async => false,
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: scaffold,
      ),
    );
  }

  @override
  void synchronize(String routeName, String task,
      [DateTime start, DateTime end]) {
    Navigator.pushReplacementNamed(context, routeName,
        arguments: WidgetArguments(task, start, end));
  }

  void _addHMW(session, text) {
    Firestore.instance
        .collection('sessions')
        .document(session.code)
        .updateData(<String, dynamic>{
      'hmw': text,
    });
  }

  void _continueSession(session) {
    DateTime now = DateTime.now();
    DateTime waitSolutionwritingStart = now;
    DateTime waitSolutionwritingEnd = waitSolutionwritingStart
        .add(Synchronizer.instance.session.waitDuration);
    DateTime writeSolutionsStart = waitSolutionwritingEnd;
    DateTime writeSolutionsEnd =
        writeSolutionsStart.add(Synchronizer.instance.session.writeDuration);
    DateTime waitSolutionvotingStart = writeSolutionsEnd;
    DateTime waitSolutionvotingEnd =
        waitSolutionvotingStart.add(Synchronizer.instance.session.waitDuration);
    DateTime voteSolutionsStart = waitSolutionvotingEnd;
    DateTime voteSolutionsEnd =
        voteSolutionsStart.add(Synchronizer.instance.session.voteDuration);

    final DocumentReference sessionRef =
        Firestore.instance.collection('sessions').document(session.code);
    sessionRef.get().then((DocumentSnapshot ds) {
      Map<dynamic, dynamic> timestamps = ds.data['timestamps'];
      Map<dynamic, dynamic> next = {
        'wait_solutionwriting': {
          'start': waitSolutionwritingStart,
          'end': waitSolutionwritingEnd,
        },
        'write_solutions': {
          'start': writeSolutionsStart,
          'end': writeSolutionsEnd,
        },
        'wait_solutionvoting': {
          'start': waitSolutionvotingStart,
          'end': waitSolutionvotingEnd,
        },
        'vote_solutions': {
          'start': voteSolutionsStart,
          'end': voteSolutionsEnd,
        },
      };
      timestamps.addAll(next);
      sessionRef.updateData(<String, dynamic>{
        'timestamps': timestamps,
        'on_hold': 'results',
      });
    });
  }
}
