import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';
import 'package:decisionjamapp/util/session.dart';
import 'package:decisionjamapp/util/synchronizable.dart';

/// Synchronizes a [Synchronizable] widget based on the Firestore database.
///
/// Implements the Singleton design pattern and a variation of the Observer
/// pattern.
class Synchronizer {
  /// Private constructor
  Synchronizer._constructor();

  /// The default [Synchronizer] instance
  static final Synchronizer instance = Synchronizer._constructor();

  /// Provides the [code] and [moderator] variables
  ///
  /// [code] - Firestore's document id of the current session
  /// [moderator] - Whether or not the current user is a moderator or a participant
  Session session;

  /// Queue for widgets to be synchronized.
  ///
  /// The first item in this will be synchronized if [notify] is called.
  final Queue<Synchronizable> _queue = Queue<Synchronizable>();

  /// Current timestamps
  Map<dynamic, dynamic> _timestamps = {};

  /// Used to see if the timestamps in the database are different from [_timestamps]
  final Function deepCollectionEquality = const DeepCollectionEquality().equals;

  /// Current on_hold String
  String onHold;

  /// A list of every route (without a leading '/') that a moderator should be
  /// sent to when the session is on hold.
  final List<String> moderatorOnHoldRoutes = [
    'hmw',
    'results'
  ];

  /// A list of every route (without a leading '/') that a participant should be
  /// sent to when the session is on hold.
  final List<String> participantOnHoldRoutes = [
    'results'
  ];

  /// Adds a [Synchronizable] to [_queue].
  void add(Synchronizable widget) {
    print('Synchronizer: Added ' + widget.toString() + ' to queue.');
    _queue.add(widget);
  }

  /// Removes and returns the first [Synchronizable] from [_queue].
  Synchronizable pop() {
    print('Synchronizer: popped ' + _queue.first.toString() + ' from queue.');
    return _queue.removeFirst();
  }

  /// Synchronizes the first [Synchronizable] in [_queue].
  ///
  /// Iterates over every element in [_timestamps] and checks if it's currently
  /// running. Pops the first [Synchronizable] from [_queue] and synchronizes it.
  ///
  /// If there is no running task, the app gets routed to /hold with a message.
  ///
  /// If [session] is null, an exception is thrown.
  void notify() {
    if (session == null) {
      throw Exception('No session set.');
    }
    bool synchronized = false;
    if (_queue.length == 0) {
      return;
    }
    if (_timestamps.isNotEmpty) {
      _timestamps.forEach((section, timestamps) {
        DateTime now = DateTime.now();
        if (timestamps['start'].toDate().isBefore(now) &&
            now.isBefore(timestamps['end'].toDate())) {
          synchronized = true;
          String routeName = '/' + section.split('_')[0];
          String task = section.split('_')[1];
          DateTime start = timestamps['start'].toDate();
          DateTime end = timestamps['end'].toDate();
          print('Synchronizer: ' +
              _queue.first.toString() +
              ' synchronized and popped.');
          _queue.removeFirst().synchronize(routeName, task, start, end);
        }
      });
    }
    if (!synchronized) {
      if (session.moderator) {
        if (moderatorOnHoldRoutes.contains(onHold)) {
          _queue.removeFirst().synchronize('/' + onHold, onHold);
        }
      } else {
        if (participantOnHoldRoutes.contains(onHold)) {
          _queue.removeFirst().synchronize('/' + onHold, onHold);
        }
        else {
          _queue.removeFirst().synchronize('/hold', onHold);
        }
      }
    }
  }

  /// Creates a [Stream] from the 'sessions/<code>' and iterates over it,
  /// calling [notify] every time the timestamps or on_hold changes.
  ///
  /// If [session] is null, an exception is thrown.
  void listen() async {
    if (session == null) {
      throw Exception('No session set.');
    }
    Stream<DocumentSnapshot> stream = Firestore.instance
        .collection('sessions')
        .document(session.code)
        .snapshots();

    await for (DocumentSnapshot snapshot in stream) {
      if (snapshot.exists) {
        if (snapshot.data['timestamps'] != null &&
            !deepCollectionEquality(_timestamps, snapshot.data['timestamps'])) {
          _timestamps = snapshot.data['timestamps'];
          notify();
        }
        if (onHold != snapshot.data['on_hold']) {
          onHold = snapshot.data['on_hold'];
          notify();
        }
      }
      else {
        break;
      }
    }
  }
}
