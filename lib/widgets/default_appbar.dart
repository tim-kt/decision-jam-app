import 'package:flutter/material.dart';

/// Blue [AppBar] displaying "Decision Jam App"
class DefaultAppBar extends StatelessWidget implements PreferredSizeWidget{
  DefaultAppBar({Key key}) : preferredSize = Size.fromHeight(56.0), super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  /// Builds the [AppBar] with a blue background and
  /// the text "Decision Jam App"
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text('Decision Jam App'),
      backgroundColor: Colors.blue[600],
    );
  }
}
