import 'package:flutter/material.dart';

import 'package:decisionjamapp/screens/entry.dart';
import 'package:decisionjamapp/util/synchronizer.dart';

class MenuScaffold extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Decision Jam App'),
        backgroundColor: Colors.blue[600],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ButtonTheme(
              minWidth: 180.0,
              height: 60.0,
              child: RaisedButton(
                child: Text('Moderator', style: TextStyle(fontSize: 18)),
                color: Colors.blue[600],
                textColor: Colors.white,
                padding: const EdgeInsets.all(0.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(32.0))),
                splashColor: Colors.transparent,
                onPressed: () {
                  pushEntry(context, true);
                },
              ),
            ),
            SizedBox(height: 16.0),
            ButtonTheme(
              minWidth: 180.0,
              height: 60.0,
              child: RaisedButton(
                child: Text('Participant', style: TextStyle(fontSize: 18)),
                color: Colors.blue[600],
                textColor: Colors.white,
                padding: const EdgeInsets.all(0.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(32.0))),
                splashColor: Colors.transparent,
                onPressed: () {
                  pushEntry(context, false);
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  /// Passes the current BuildContext to the [Synchronizer] and pushes the
  /// EntryScaffold widget
  void pushEntry(BuildContext context, bool moderator) async {
    Navigator.pushNamed(
      context,
      EntryScreen.routeName,
      arguments: EntryArguments(moderator),
    );
  }
}
