import 'package:flutter/material.dart';

/// [Card] with an [_elevation] displaying a String [_information]
class InformationCard extends StatelessWidget {
  final String _information;
  final double _elevation;

  InformationCard(this._information, this._elevation);

  /// Builds the [Card] with a centered and "padded" [Text]
  /// with the fontSize 18.0 and the given [_elevation]
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Center(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Text(
            _information ?? '',
            textAlign: TextAlign.center,
          ),
        ),
      ),
      elevation: _elevation,
    );
  }
}
