import 'dart:async';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:decisionjamapp/util/synchronizable.dart';
import 'package:decisionjamapp/util/synchronizer.dart';
import 'package:decisionjamapp/util/widget_arguments.dart';
import 'package:decisionjamapp/widgets/progress_indicator_container.dart';
import 'package:decisionjamapp/widgets/information_card.dart';

class WriteScaffold extends StatefulWidget {
  static const routeName = '/write';

  @override
  _WriteScaffoldState createState() => _WriteScaffoldState();
}

class _WriteScaffoldState extends State<WriteScaffold>
    with SingleTickerProviderStateMixin
    implements Synchronizable {
  final List<String> _written = List<String>();
  final TextEditingController _controller = TextEditingController();
  String hmw = '';

  @override
  void initState() {
    super.initState();
    Synchronizer.instance.add(this);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final WidgetArguments args = ModalRoute.of(context).settings.arguments;
    Timer(args.end.difference(DateTime.now()), () {
      Synchronizer.instance.notify();
    });
  }

  @override
  Widget build(BuildContext context) {
    final WidgetArguments args = ModalRoute.of(context).settings.arguments;
    Scaffold scaffold = Scaffold(
      appBar: AppBar(
        title: Text('Decision Jam App'),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        children: <Widget>[
          _getTask(Synchronizer.instance.session, args.task),
          ProgressIndicatorContainer(args.start, args.end),
          Expanded(
            child: Container(
              child: ListView.builder(
                itemCount: _written.length,
                itemBuilder: (context, position) {
                  return Card(
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(_written[position]),
                    ),
                  );
                },
              ),
            ),
          ),
          Container(
            color: Colors.grey[200],
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: TextField(
                  controller: _controller,
                  onEditingComplete: () {
                    if (_controller.text.isNotEmpty) {
                      addTextToFirestore(Synchronizer.instance.session,
                          args.task, _controller.text);
                      setState(() {
                        _written.add(_controller.text);
                        _controller.clear();
                      });
                    }
                  },
                  minLines: 1,
                  maxLines: 5,
                  textInputAction: TextInputAction.send,
                  decoration: InputDecoration(
                    hintText: 'Type here...',
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
    return WillPopScope(
      onWillPop: () async => false,
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: scaffold,
      ),
    );
  }

  @override
  void synchronize(String routeName, String task,
      [DateTime start, DateTime end]) {
    Navigator.pushReplacementNamed(context, routeName,
        arguments: WidgetArguments(task, start, end));
  }

  void addTextToFirestore(session, task, text) {
    Firestore.instance
        .collection('sessions/' + session.code + '/' + task)
        .document()
        .setData(<String, dynamic>{
      'content': text,
      'votes': 0,
    });
  }

  Widget _getTask(session, placeholder) {
    String task;
    if (placeholder == 'problems') {
      task =
          'Please write down all the problems you are currently having with the project.';
    }
    if (placeholder == 'solutions') {
      if (hmw == '') {
        _getHMW(session);
      }
      task =
          'Please write down all the solutions you can think of to the following question:\n' +
              hmw;
    }
    return InformationCard(
      task,
      2.0,
    );
  }

  void _getHMW(session) async {
    /// Get objects from Firestore
    await Firestore.instance
        .collection('sessions')
        .document(session.code)
        .get()
        .then((DocumentSnapshot ds) {
      hmw = ds.data['hmw'];
      setState(() {});
    });
  }
}
